<?php
class Database
{
    private $mysqli;

    private $result;

    private $fetchResult = array();

    public function __construct($host, $user, $password, $database)
    {
        $this->mysqli = new mysqli($host, $user, $password, $database);

        if ($this->mysqli->connect_errno)
        {
            echo "Failed to connect to MySQL: (" . $this->mysqli->connect_errno . ") " . $this->mysqli->connect_error;
        }
    }

    public function __destruct()
    {
    }

    public function SetConnection($host, $user, $password, $database)
    {
        $this->mysqli = new mysqli($host, $user, $password, $database);

        if ($this->mysqli->connect_errno)
        {
            echo "Failed to connect to MySQL: (" . $this->mysqli->connect_errno . ") " . $this->mysqli->connect_error;
        }
    }

    public function Query($query)
    {
        $this->result = $this->mysqli->query($query);
    }

    public function GetResult()
    {
        // ToDo wenn es nur ein Ergebnis gibt dann soll nur das rausgegeben werden statt ein array

        while($result = mysqli_fetch_assoc($this->result))
        {
            $this->fetchResult[] = $result;
        }

        return $this->fetchResult;
    }

    public function ResultExists()
    {
        if($this->result)
        {
            return true;
        }
        return false;
    }
}

?>