<?php

// [sz] in ß
function changeVocalOutput($string)
{
    return str_replace(array("[sz]", "[ae]", "[AE]", "[oe]", "[OE]", "[ue]", "[UE]", "[at]"), array("ß", "ä", "Ä", "ö", "Ö", "ü", "Ü", "@"), $string);
}

// ß in [sz]
function changeVocalInput($string)
{
    return str_replace(array("ß", "ä", "Ä", "ö", "Ö", "ü", "Ü", "@"), array("[sz]", "[ae]", "[AE]", "[oe]", "[OE]", "[ue]", "[UE]", "[at]"), $string);
}
//Passwort Verschlüsslungsfunktion 
function encrypt_password($password)
{
	$secret_salt = "Sup3rS3e7y";
	$salted_password = $secret_salt . $password;
	$password_hash = hash('sha256', $salted_password);

	return $password_hash;
}
function validate($string) 
{
		return htmlspecialchars(strip_tags(stripslashes(trim($string))));
}
function changePassword() {
		$showdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
		if (!isset($_POST["newPW"]) OR !isset($_POST["oldPW"]) OR !isset($_POST["passwordcheck"])) {
			echo '<div class="alert alert-danger" role="alert">Bitte fülle alle Felder aus</div>';
		} else if ($_POST["newPW"] != $_POST["passwordcheck"])
		{
			echo '<div class="alert alert-danger" role="alert">Das neue Passwort stimmt nicht mit der Wiederholung überein</div>';
		} else {
		
	
			$query = "SELECT password, username FROM duser WHERE id = '".USER_ID."'";
			$showdata->Query($query);
			if ($showdata->ResultExists()) {
				$obj = $showdata->GetResult()[0]; 
				$name = $obj['username']; 	
					if ($obj['password'] == encrypt_password(validate($_POST["oldPW"]))) {
						$newPW = encrypt_password($_POST["newPW"]);
						$query = "UPDATE duser SET password = '".$newPW."' WHERE id ='".USER_ID."';";
						$showdata->Query($query);
						echo '<div class="alert alert-success" role="alert">Erfolgreich geändert</div>';
							?>
						<script type="text/javascript">
							window.setTimeout('location.href="http://test.moddylp.de/websitetest/index.php"', 3000);
						</script>
						<?php
					} else {
						echo '<div class="alert alert-danger" role="alert">Ändern fehgeschlagen</div>';
					}
					
			}
		}
	}