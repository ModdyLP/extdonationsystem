<?php if(!defined('SCRIPT_VALID')) DIE("Du hast keine Berechtigung!"); /*Pr�ft ob es mit index.php ge�ffnet wurde*/?>
<?php	 
		$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);	
		// der nutzer => normal weiter
		if (isset($_POST["from"]) AND isset($_POST["to"]) AND $_POST["amount"] > 0) {
			
			$fromNum = 0;
			switch ($_POST["from"]) {
				case "Gold":
					$fromNum = 3;
					break;
				case "Silber":
					$fromNum = 2;
					break;
				case "Bronze":
					$fromNum = 1;
					break;
			}
			
			$toNum = 0;
			switch ($_POST["to"]) {
				case "Gold":
					$toNum = 3;
					break;
				case "Silber":
					$toNum = 2;
					break;
				case "Bronze":
					$toNum = 1;
					break;
			}
			
			$from = $_POST["from"];
			$to = $_POST["to"];
			$fromLower = strtolower($from);
			$toLower = strtolower($to);
			
			$delta = $fromNum - $toNum;
			
			
			$query = "SELECT $fromLower, $toLower FROM duser WHERE id = '".USER_ID."'"; 
			$getdata->Query($query);
			if ($getdata->ResultExists()) {
				$obj = $getdata->GetResult()[0];
			} else {
				echo '<p class="bg-error">Error</p>';
				die();
			}
			
			$amount = $_POST[amount];
			$newFromAmount = $obj[$fromLower] - $amount;
				
			if ($delta == -1) {
				// von tief nach hoch: 20:1
				$toAmount = $amount / 20;
			}
			else if ($delta == 0) {
				echo '<div class="alert alert-danger" role="alert">Du kannst nicht disselbe Währung tauschen</div>';
				?>
					<script type="text/javascript">
						window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=detail&action=<?php echo USER_ID; ?>"', 1000);
					</script>
					<?php
					die();
			}
			else if ($delta == -2) {
				// von hoch nach tief: 20:1
				$toAmount = $amount / 400;
			}
			else if ($delta == 1) {
				// von hoch nach tief: 1:10
				$toAmount = $amount * 10;
			} else if ($delta == 2) {
				// von hoch nach tief: 1:10
				$toAmount = $amount * 100;
			}
			else {
				// FEHLER ungueltige diff
			}
			$toAmount = number_format($toAmount, 2, '.', '');
			$newToAmount = $obj[$toLower] + $toAmount;
			
			if ($toAmount <= 0 OR $obj[$fromLower] <= $toAmount) {
				echo '<div class="alert alert-danger" role="alert">Nicht genügend '.$from.' zum tauschen</div>';
				?>
					<script type="text/javascript">
						window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=detail&action=<?php echo USER_ID; ?>"', 1000);
					</script>
					<?php
			}
			else {
					$query = "INSERT INTO changelog (id, username, changed, amount, reason, timestamp) VALUES (NULL, '".USER_ID."', '$from', '-$_POST[amount]', '$amount $from zu $toAmount $to', CURRENT_TIMESTAMP)";
					$getdata->Query($query);
				
					$changed = strtolower($_POST["changed"]);
					$query = "UPDATE duser SET $from = '$newFromAmount' WHERE id = '".USER_ID."'";
					$getdata->Query($query);
				
					$query = "INSERT INTO changelog (id, username, changed, amount, reason, timestamp) VALUES (NULL, '".USER_ID."', '$to', '$toAmount', '$amount $from zu $toAmount $to', CURRENT_TIMESTAMP)";
					$getdata->Query($query);
		
					$changed = strtolower($_POST["changed"]);
					$query = "UPDATE duser SET $to = '$newToAmount' WHERE id ='".USER_ID."'";
					$getdata->Query($query);
			echo '<div class="alert alert-success" role="alert">Erfolgreich '.$amount.' '.$from.' in '.$toAmount.' '.$to.' umgetauscht.</div>';
			?>
			<script type="text/javascript">
				window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=detail&action=<?php echo USER_ID; ?>"', 2000);
			</script>
			<?php
			}
		}
?>