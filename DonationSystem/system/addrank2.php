<?php if(!defined('SCRIPT_VALID')) DIE("Du hast keine Berechtigung!"); /*Prüft ob es mit index.php geöffnet wurde*/?>
<?php
	if (!isset($_POST["send2"])) {
		include "system/addrankform2.php";
	} else {
		$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
		$getdata->Query("SELECT * FROM pdranks WHERE id = '".$_GET['id']."';");
		if ($getdata->ResultExists()) {
			$getdata2 = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
			$getdata2->Query("SELECT * FROM rankusercon WHERE rankid = '".$_GET['id']."' AND userid = '".USER_ID."';");
			if ($getdata2->ResultExists() AND $getdata2->GetResult()[0] != null) {
				$rank2 = $getdata2->GetResult()[0];
				$state = $rank2['state'];
			} else {
				$state = 0;
			}
			$rank = $getdata->GetResult()[0];
				if ($state >= $rank['goal']) {
					echo '<div class="alert alert-danger" role="alert">Diese Freischaltung ist bereits abgeschlossen</div>';
					?>
					<script type="text/javascript">
						window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=ranks"', 3000);
					</script>
					<?php
					die();
				} else {
					if (isset($_POST['amount'])) {
						$amount = $_POST['amount'];
						$left = $rank["goal"]-$state;
						if ($left <= $amount) {
							$amount = $left;
						}
						$getdata2 = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
						$getdata2->Query("SELECT * FROM duser WHERE id = '".USER_ID."';");
						if ($getdata->ResultExists()) {
							$user = $getdata2->GetResult()[0];
							$art = strtolower($rank['art']);
							if ($user[$art] >= $amount) {
								$newamount = $user[$art]-$amount;
								$savedata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
								if ($state == 0) {
									$state = $state+$amount;
									$savedata->Query("INSERT INTO rankusercon (id, rankid, userid, timestamp, state) VALUES (NULL, '".$_GET['id']."', '".USER_ID."', NULL, '".$state."');");
								} else {
									$state = $state+$amount;
									$savedata->Query("UPDATE rankusercon SET state = '".$state."' WHERE rankid = '".$_GET['id']."' AND userid = '".USER_ID."'");
								}
								$savedata->Query("UPDATE duser SET ".$art." = '".$newamount."' WHERE id = '".USER_ID."'");
								$savedata->Query(changeVocalInput("INSERT INTO changelog (id, username, changed, amount, reason, timestamp) VALUES (NULL, '".USER_ID."', '".$rank['art']."', '-".$amount."', 'Spende an: ".$rank['name']." Freischaltung', CURRENT_TIMESTAMP)"));
								echo '<div class="alert alert-success" role="alert">Vielen Dank für dein Beitrag('.$amount.'), ihr seid eurem Ziel ein Stück näher</div>';
								?>
								<script type="text/javascript">
									window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=ranks"', 3000);
								</script>
								<?php
							} else {
								echo '<div class="alert alert-danger" role="alert">Du hast nicht genügend '.$rank['art'].'</div>';
								?>
								<script type="text/javascript">
									window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=ranks"', 3000);
								</script>
								<?php
								die();
							}
						}
					} else {
						echo '<div class="alert alert-danger" role="alert">Du musst einen Betrag eingeben</div>';
						?>
						<script type="text/javascript">
							window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=ranks"', 3000);
						</script>
						<?php
						die();
					}		
				}
			}
	}