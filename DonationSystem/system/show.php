<?php if(!defined('SCRIPT_VALID')) DIE("Du hast keine Berechtigung!"); /*Prüft ob es mit index.php geöffnet wurde*/?>
<?php		
		$showdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
		
		$query = "SELECT * FROM duser"; 

		if (isset($_GET["search"])) {
			$search = $_GET["search"];
			$query .= " WHERE username LIKE '%" . $search . "%'";
		}
	?>
	<div class="panel panel-default">
	  <!-- Default panel contents -->
		<div class="panel-heading">
				Aktuelle Bilanzen
		</div>
		<div class="panel-body">
			 <form class="form-inline" method="GET">
			  <div class="form-group">
				<label for="search">Namen Suche</label>
				<input type="text" class="form-control" name="search" placeholder="Name">
				<input type="submit" class="btn btn-primary" value="Suchen">
			  </div>
			</form>
		</div>
	<?php
		if (isset($_GET["order"])) {
			$query .= " ORDER BY " . $_GET["order"];
		
			if (isset($_GET["desc"]) && $_GET["desc"] == 1) {
				$query .= " DESC";
			}
		}
		$showdata->Query($query);
	if ($showdata->ResultExists()) {
	?>
	<table class="table">
	<tr>
	<?php
			$link = "";
			if (isset($search)) {
				$link = "&search=$search";
			}
			
			if (isset($_GET["order"])) {
				$order = $_GET["order"];
				$desc = 0;
				if (isset($_GET["desc"]) && $_GET["desc"] == 1) {
					$desc = 1;
				}
				
				
				if (strcasecmp($order, "username") == 0) {
					$desc = abs($desc - 1);
					echo "<th><a href=?order=username&desc=$desc$link>Username</a></th>";
				}
				else {
					echo "<th><a href=?order=username$link>Username</a></th>";
				}
				
				if (strcasecmp($order, "Gold") == 0) {
					$desc = abs($desc - 1);
					echo "<th><a href=?order=gold&desc=$desc$link><img src='./img/Gold.gif'/> Gold</a></th>";
				}
				else {
					echo "<th><a href=?order=gold$link><img src='./img/Gold.gif'/> Gold</a></th>";
				}
				
				if (strcasecmp($order, "Silber") == 0) {
					$desc = abs($desc - 1);
					echo "<th><a href=?order=silber&desc=$desc$link><img src='./img/Silber.gif'/> Silber</a></th>";
				}
				else {
					echo "<th><a href=?order=silber$link><img src='./img/Silber.gif'/> Silber</a></th>";
				}
				
				if (strcasecmp($order, "Bronze") == 0) {
					$desc = abs($desc - 1);
					echo "<th><a href=?order=bronze&desc=$desc$link><img src='./img/Bronze.gif'/> Bronze</a></th>";
				}
				else {
					echo "<th><a href=?order=bronze$link><img src='./img/Bronze.gif'/> Bronze</a></th>";
				}
			}
			else {
				echo "<th><a href=?order=username$link>Username</a></th>";
				echo "<th><a href=?order=gold$link><img src='./img/Gold.gif'/> Gold</a></th>";
				echo "<th><a href=?order=silber$link><img src='./img/Silber.gif'/> Silber</a></th>";
				echo "<th><a href=?order=bronze$link><img src='./img/Bronze.gif'/> Bronze</a></th>";
			}
	?>
		</tr>
	<?php
			foreach ($showdata->GetResult() as $obj) {
				if ($obj['active'] == 1) {
					echo "<tr>";
						echo "<td><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME.'=detail&'.GET_ACTION_NAME.'='.$obj['id']."'>".$obj['username']."</a></td>";
						echo "<td>".$obj['gold']."</td>";
						echo "<td>".$obj['silber']."</td>";
						echo "<td>".$obj['bronze']."</td>";
					echo "</tr>";
				}				
			}
		echo "</table></div>";
	if (checkaccess("ADMIN")) { ?>
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	<div class="panel-heading">
		Aktuelle Bilanzen (Inaktive Nutzer)
		</div>
	<table class="table">
	<?php 
	$link = "";
			if (isset($search)) {
				$link = "&search=$search";
			}
			
			if (isset($_GET["order"])) {
				$order = $_GET["order"];
				$desc = 0;
				if (isset($_GET["desc"]) && $_GET["desc"] == 1) {
					$desc = 1;
				}
				
				
				if (strcasecmp($order, "username") == 0) {
					$desc = abs($desc - 1);
					echo "<th><a href=?order=username&desc=$desc$link>Username</a></th>";
				}
				else {
					echo "<th><a href=?order=username$link>Username</a></th>";
				}
				
				if (strcasecmp($order, "Gold") == 0) {
					$desc = abs($desc - 1);
					echo "<th><a href=?order=gold&desc=$desc$link><img src='./img/Gold.gif'/> Gold</a></th>";
				}
				else {
					echo "<th><a href=?order=gold$link><img src='./img/Gold.gif'/> Gold</a></th>";
				}
				
				if (strcasecmp($order, "Silber") == 0) {
					$desc = abs($desc - 1);
					echo "<th><a href=?order=silber&desc=$desc$link><img src='./img/Silber.gif'/> Silber</a></th>";
				}
				else {
					echo "<th><a href=?order=silber$link><img src='./img/Silber.gif'/> Silber</a></th>";
				}
				
				if (strcasecmp($order, "Bronze") == 0) {
					$desc = abs($desc - 1);
					echo "<th><a href=?order=bronze&desc=$desc$link><img src='./img/Bronze.gif'/> Bronze</a></th>";
				}
				else {
					echo "<th><a href=?order=bronze$link><img src='./img/Bronze.gif'/> Bronze</a></th>";
				}
			}
			else {
				echo "<th><a href=?order=username$link>Username</a></th>";
				echo "<th><a href=?order=gold$link><img src='./img/Gold.gif'/> Gold</a></th>";
				echo "<th><a href=?order=silber$link><img src='./img/Silber.gif'/> Silber</a></th>";
				echo "<th><a href=?order=bronze$link><img src='./img/Bronze.gif'/> Bronze</a></th>";
			}
			?>
			</tr>
			<?php
			foreach ($showdata->GetResult() as $obj) {
				if ($obj['active'] == 0) {
					echo "<tr class='warning'>";
						echo "<td><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME.'=detail&'.GET_ACTION_NAME.'='.$obj['id']."'>".$obj['username']."</a></td>";
						echo "<td>".$obj['gold']."</td>";
						echo "<td>".$obj['silber']."</td>";
						echo "<td>".$obj['bronze']."</td>";
					echo "</tr>";
				}				
			}
		}
		echo "</table></div>";
	}		
	?>
	