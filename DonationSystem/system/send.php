<?php if(!defined('SCRIPT_VALID')) DIE("Du hast keine Berechtigung!"); /*Pr�ft ob es mit index.php ge�ffnet wurde*/?>
<?php
if (checkaccess("GAST")) {
	if (isset($_POST["recipent"]) && $_POST["amount"] > 0) {
		$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
		$getdata->Query("SELECT bronze, username FROM duser WHERE id = '".$_POST['recipent']."';"); 
		$amount = number_format($_POST['amount'], 2, '.', '');
		if ($getdata->ResultExists()) {
			$obj = $getdata->GetResult()[0];
			$newamountRec = $obj['bronze'] + $amount;
			$newamountRec = number_format($newamountRec, 2, '.', '');
			$recName = $obj["username"];
			$getdata2 = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);			
			$getdata2->Query("SELECT bronze, username FROM duser WHERE id = '".USER_ID."';"); 
			if ($getdata2->ResultExists()) {
				$obj2 = $getdata2->GetResult()[0];
				$newamountSen = $obj2['bronze'] - $amount;
				$newamountSen = number_format($newamountSen, 2, '.', '');
				$senName = $obj2["username"];
				
				if ($newamountSen < 0 OR $senName == $recName OR $amount < 0.01) {
					echo '<div class="alert alert-danger" role="alert">Du hast nicht genügend Bronze oder du versuchst dir selber etwas zu senden oder du hast zu wenig eingegeben('.$amount.')</div>';
					?>
					<script type="text/javascript">
						window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=detail&action=<?php echo USER_ID; ?>"', 2000);
					</script>
					<?php
				}
				else 
				{
					$getdata->Query(changeVocalInput("INSERT INTO changelog (id, username, changed, amount, reason, timestamp) VALUES (NULL, '".$_POST['recipent']."', 'Bronze', '".$amount."', 'Von ".$senName.": ". validate($_POST['reason'])."', CURRENT_TIMESTAMP)"));
						
					$getdata->Query("UPDATE duser SET bronze = '$newamountSen' WHERE id = '".$_GET['id']."'");
					
					$getdata->Query(changeVocalInput("INSERT INTO changelog (id, username, changed, amount, reason, timestamp) VALUES (NULL, '".$_GET['id']."', 'Bronze', '-".$amount."', 'An ".$recName.": ". validate($_POST['reason'])."', CURRENT_TIMESTAMP)"));
						
					$getdata->Query("UPDATE duser SET bronze = '$newamountRec' WHERE id = '".$_POST['recipent']."'");

				
					echo '<div class="alert alert-success" role="alert">Bronze wurde erfolgreich versendet</div>';
					?>
					<script type="text/javascript">
						window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=detail&action=<?php echo USER_ID; ?>"', 2000);
					</script>
					<?php
				}
			} else {
				echo '<div class="alert alert-danger" role="alert">Absender existiert nicht</div>';
				?>
					<script type="text/javascript">
						window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=detail&action=<?php echo USER_ID; ?>"', 2000);
					</script>
					<?php
			}
		} else {
			echo '<div class="alert alert-danger" role="alert">Empfänger existiert nicht</div>';
			?>
					<script type="text/javascript">
						window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=detail&action=<?php echo USER_ID; ?>"', 2000);
					</script>
					<?php
		}
	} else {
		echo '<div class="alert alert-danger" role="alert">Empfänger und Menge muss angegeben sein</div>';
		?>
					<script type="text/javascript">
						window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=detail&action=<?php echo USER_ID; ?>"', 2000);
					</script>
					<?php
	}
}
?>