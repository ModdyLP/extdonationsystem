<?php if(!defined('SCRIPT_VALID')) DIE("Du hast keine Berechtigung!"); /*Prüft ob es mit index.php geöffnet wurde*/?>
<?php
	if (!isset($_POST["send2"])) {
		include "system/addrankform.php";
	} else {
		$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
		$getdata->Query("SELECT * FROM dranks WHERE id = '".$_GET['id']."';");
		if ($getdata->ResultExists()) {
			$rank = $getdata->GetResult()[0];
			if ($rank['state'] >= $rank['goal']) {
				echo '<div class="alert alert-danger" role="alert">Diese Freischaltung ist bereits abgeschlossen</div>';
				?>
				<script type="text/javascript">
					window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=ranks"', 3000);
				</script>
				<?php
				die();
			} else {
				if (isset($_POST['amount'])) {
					$amount = $_POST['amount'];
					$left = $rank["goal"]-$rank["state"];
					if ($left <= $amount) {
						$amount = $left;
					}
					$getdata2 = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
					$getdata2->Query("SELECT * FROM duser WHERE id = '".USER_ID."';");
					if ($getdata->ResultExists()) {
						$user = $getdata2->GetResult()[0];
						$art = strtolower($rank['art']);
						if ($user[$art] >= $amount) {
							$state = $rank["state"]+$amount;
							$newamount = $user[$art]-$amount;
							$savedata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
							$savedata->Query("UPDATE dranks SET state = '".$state."' WHERE id = '".$_GET['id']."'");
							$savedata->Query("UPDATE duser SET ".$art." = '".$newamount."' WHERE id = '".USER_ID."'");
							$savedata->Query(changeVocalInput("INSERT INTO changelog (id, username, changed, amount, reason, timestamp) VALUES (NULL, '".USER_ID."', '".$rank['art']."', '-".$amount."', 'Spende an: ".$rank['name']." Freischaltung', CURRENT_TIMESTAMP)"));
							echo '<div class="alert alert-success" role="alert">Vielen Dank für dein Beitrag('.$amount.'), ihr seid eurem Ziel ein Stück näher</div>';
							?>
							<script type="text/javascript">
								window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=ranks"', 3000);
							</script>
							<?php
						} else {
							echo '<div class="alert alert-danger" role="alert">Du hast nicht genügend '.$rank['art'].'</div>';
							?>
							<script type="text/javascript">
								window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=ranks"', 3000);
							</script>
							<?php
							die();
						}
					}
				} else {
					echo '<div class="alert alert-danger" role="alert">Du musst einen Betrag eingeben</div>';
					?>
					<script type="text/javascript">
						window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=ranks"', 3000);
					</script>
					<?php
					die();
				}		
			}
		} else  {
			echo '<div class="alert alert-danger" role="alert">Diese Freischaltung ist bereits abgeschlossen</div>';
			?>
			<script type="text/javascript">
				window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=ranks"', 3000);
			</script>
			<?php
			die();
		}
		
		
	}