<?php if(!defined('SCRIPT_VALID')) DIE("Du hast keine Berechtigung!"); /*Pr�ft ob es mit index.php ge�ffnet wurde*/?>
<h1> Erstelle einen Account</h1>
<form action="<?php echo $_SERVER["PHP_SELF"].'?'.GET_MODULE_NAME.'=registeruser'; ?>" method="post">
	<div class="form-group">
		<label for="Username">Username</label>
		<input type="text" class="form-control" name="username" placeholder="Username" required="true">
	</div>
	<div class="form-group">
		<label for="email">Email</label>
		<input type="email" class="form-control" name="email" required="true">
	</div>
	<div class="form-group">
		<label for="password">Passwort</label>
		<input type="password" class="form-control" name="password" placeholder="Passwort" required="true">
	</div>
	<div class="form-group">
		<label for="passwordcheck">Passwort Wiederholung</label>
		<input type="password" class="form-control" name="passwordcheck" placeholder="Passwort wiederholen" required="true">
	</div>
	<div class="form-group">
		<button type="submit" name="send2" value="Register" class="btn btn-default">Registrieren</button>
	</div>
</form>