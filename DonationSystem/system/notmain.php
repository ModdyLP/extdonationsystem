<?php if(!defined('SCRIPT_VALID')) DIE("Du hast keine Berechtigung!"); /*Prüft ob es mit index.php geöffnet wurde*/?>
<?php
//Errror Array initialisieren
$errors = array();
//Überprüfung ob ein Cookie namens Email vorhanden ist.
if (isset($_COOKIE["login"]) AND $_COOKIE["login"] == "success") 
	{
		echo '<div class="alert alert-success" role="alert">Login erfolgreich</div>';
		setcookie("login",null, -1);
	}
	if (isset($_COOKIE["login"]) AND $_COOKIE["login"] == "logout") 
	{
		echo '<div class="alert alert-success" role="alert">Logout erfolgreich</div>';
		setcookie("login",null, -1);
	}
if (!isset($_COOKIE["key"]))
{
	//Error Array füllen mit einer Fehlermeldung
	$errors[] = "Sie sind nicht angemeldet.";
	//Loginformular einbinden
	include_once "./system/loginform.php";
}
else
{
	//Wenn ein Cookie vorhanden ist dann Abfrage von Cookiedaten aus der Datenbank
	$proofcookie = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
	$proofcookie->Query("SELECT keystamp FROM duser WHERE keystamp = '".$_COOKIE["key"]."';");
	//Überprüfung der Cookiedaten auf Existens
	if ($proofcookie->ResultExists() == 1) 
		{
			foreach($proofcookie->GetResult() as $proofcookieF)
				{
				}
			//Cokkie Inhalt überprüfen und Cokkie bei Veränderung leeren
			if (!isset($proofcookieF)) 
			{
			}
			if ($proofcookieF["keystamp"] != $_COOKIE["key"])
			{
				setcookie("time",null, -1);
				setcookie("key", null, time()+3600);
				setcookie("lastlogin", null, -1);
				setcookie("expire",null, -1);
				setcookie("firstname",null, -1);
				//Header Weiterleitung Selbstaufruf zum Refreshen
				header("Location: ".$_SERVER['PHP_SELF']);
				
			}
		} 
		else
		{
			setcookie("time",null, -1);
			setcookie("key", null, time()+3600);
			setcookie("lastlogin", null, -1);
			setcookie("expire",null, -1);
			setcookie("firstname",null, -1);
			//Header Weiterleitung Selbstaufruf zum Refreshen
			header("Location: ".$_SERVER['PHP_SELF']);
		}
	//Eingeloggt Status Anzeige
	if (isset($_COOKIE["lastlogin"]) AND isset($_COOKIE["username"])) 
		{
			echo "<p class='statusmsg'>Sie sind als ".$_COOKIE["username"]." angemeldet. <br> Letzter Login: ".$_COOKIE["lastlogin"]."</p>";
		} 
		else
		{
			echo "<p class='statusmsg'>Sie sind angemeldet!</p>";
			echo "<p class='errormsg'>Es konnten keine Daten erfasst werden!</p>";
		}
	echo '<a href="'.$_SERVER["PHP_SELF"].'?'.GET_ACTION_NAME.'=logout"><button class="btn btn-default">Logout</button></a>';
}
//Login Formular auf Absende Status überprüfen
if (isset($_POST["send"]))
{
	//Eingabe Validieren
	if (empty($_POST["username"]))
	{
		$errors[] = "Sie müssen eine Email angeben";
	}
	if (empty($_POST["password"]))
	{
		$errors[] = "Sie müssen eine Passwort angeben";
	}
	//HTML Injection CXX verhindern
	$username = htmlspecialchars(changeVocalInput($_POST["username"]));
	$password = htmlspecialchars(changeVocalInput($_POST["password"]));
	//Datenbank Verbindung aufbauen und Informationen zum Login abfragen
	$getpasswords = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
	$getpasswords->Query("SELECT * FROM duser WHERE username = '".$username."';");
	//Results auf existenz prüfen
	if ($getpasswords->ResultExists() == 1) 
	{
		foreach($getpasswords->GetResult() as $getpassword)
			{
			}
		
		if (!isset($getpassword)) 
		{
			echo '<div class="alert alert-danger" role="alert">Passwort oder Benutzername falsch</div>';
		} 
		else 
		{
		//Eingabe mit Datensatz abgleichen
		if ($getpassword["active"] != 0) {
			if ($getpassword["password"] == encrypt_password($password))
				{
					if (isset($getpassword["keytstamp"]) OR $getpassword["keytstamp"] != NULL) 
					{
						$lastlogin = $getpassword["keytstamp"];
						$lastlogin = date('d.m.y H:i',$lastlogin);
					} else {
						$lastlogin = "Kein letzter Login";
					}	
						
					//Timestamp kriegen und Keystamp erzeugen
					$timestamp = time();
					$key = encrypt_password($timestamp);
					//Cookie füllen 
					setcookie("time",time(), time()+3600);
					setcookie("key",$key, time()+3600);
					setcookie("lastlogin", $lastlogin, time()+3600);
					setcookie("login","success", time()+60);
					setcookie("expire",time()+3600, time()+3600);
					setcookie("username",$getpassword["username"], time()+3600);
					//Datenbank Update des Datensatzes mit einem Timestamo und einem Keystamp
					$getpasswords->Query("UPDATE duser SET keystamp = '".$key."', keytstamp = '".$timestamp."' WHERE username = '".$username."';");
					
					//Selbstaufruf zum refreshen
					header("Location: ".$_SERVER['PHP_SELF']);
				}
				else
				{
					echo '<div class="alert alert-danger" role="alert">Passwort oder Benutzername falsch</div>';
				}
		} else {
			echo '<div class="alert alert-danger" role="alert">Du bist noch nicht aktiviert, Spende oder lasse dich aktivieren</div>';
		}
		}

			
	} 
	else
	{
		echo '<div class="alert alert-danger" role="alert">Passwort oder Benutzername falsch</div>';
	}
	
}
//Ausgabe der Fehlermeldungen
if (count($errors) > 0)
{
	foreach ($errors as $fehler)
	{
		echo '<div class="alert alert-danger" role="alert">'.$fehler.'</div>';
	}
}
if (checkaccess("GAST")) {
?>
<a href="<?php echo $_SERVER["PHP_SELF"].'?'.GET_MODULE_NAME.'=changepw'; ?>"><button type="button" class="btn btn-default">Passwort ändern</button></a>
<?php
}
if (!checkaccess("GAST")) {
?>
<a href="<?php echo $_SERVER["PHP_SELF"].'?'.GET_MODULE_NAME.'=registeruser'; ?>"><button type="button" class="btn btn-default">Registrieren</button></a>
<?php
}
?>