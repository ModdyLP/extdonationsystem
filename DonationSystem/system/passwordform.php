﻿<form action="<?php echo $_SERVER["PHP_SELF"].'?'.GET_MODULE_NAME.'=changepw'; ?>" method="post">
			<div class="form-group">
				<label for="Username">Username</label>
				<input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo $_COOKIE["username"]; ?>" required="true">
			</div>
			<div class="form-group">
				<label for="oldPW">Altes Passwort</label>
				<input type="password" class="form-control" name="oldPW" placeholder="Passwort" required="true">
			</div>
			<div class="form-group">
				<label for="newPW">Neues Passwort</label>
				<input type="password" class="form-control" name="newPW" placeholder="Passwort" required="true">
			</div>
			<div class="form-group">
				<label for="passwordcheck">Neues Passwort Wiederholung</label>
				<input type="password" class="form-control" name="passwordcheck" placeholder="Passwort wiederholen" required="true">
			</div>
			<div class="form-group">
				<button type="submit" name="send2" value="Register" class="btn btn-default">Passwort ändern</button>
			</div>
		</form>