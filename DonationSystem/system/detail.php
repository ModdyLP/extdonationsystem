<?php if(!defined('SCRIPT_VALID')) DIE("Du hast keine Berechtigung!"); /*Prüft ob es mit index.php geöffnet wurde*/?>
<?php
if (!isset($_GET[GET_ACTION_NAME])) {
		echo "<h1>Username fehlt</h1>";
	} else {
		$showdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);

		$query = "SELECT * FROM duser WHERE id = ".$_GET[GET_ACTION_NAME].";"; 

		$showdata->Query($query);
		if ($showdata->ResultExists()) {
			
		$obj = $showdata->GetResult()[0];
	if (checkaccess("ADMIN")) {	
?>
<div class="panel panel-default">
				  <div class="panel-heading">
					<h3 class="panel-title">Admin Menü</h3>
				  </div>
					<div class="panel-body">
						<a href="<?php echo $_SERVER["PHP_SELF"].'?'.GET_MODULE_NAME.'=addingot&id='.$obj["id"]; ?>"><button class="btn btn-success">Barren hinzufügen</button></a>
						<a href="<?php echo $_SERVER["PHP_SELF"].'?'.GET_MODULE_NAME.'=removeingot&id='.$obj["id"]; ?>"><button class="btn btn-warning">Barren entfernen</button></a>
						<a href="<?php echo $_SERVER["PHP_SELF"].'?'.GET_MODULE_NAME.'=delete&id='.$obj["id"]; ?>"><button class="btn btn-danger">Account löschen</button></a>
						<?php if ($obj["active"] ==0) { ?>
						<a href="<?php echo $_SERVER["PHP_SELF"].'?'.GET_MODULE_NAME.'=activate&id='.$obj["id"]; ?>"><button class="btn btn-info">Aktivieren</button></a>
						<?php } else { ?>
						<a href="<?php echo $_SERVER["PHP_SELF"].'?'.GET_MODULE_NAME.'=deactivate&id='.$obj["id"]; ?>"><button class="btn btn-warning">Deaktivieren</button></a>
						<?php } ?>
					</div>
			</div>
	<?php } ?>
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">Aktuelle Bilanzen</div>
	  <table class="table">
<?php
			
			echo "<tr><td><b>Username</b></td><td>";
			echo " ".$obj['username']."</td></tr>";
		
			echo "<tr><td><img src='img/Gold.gif'/> <b>Gold</b></td><td> ";
			echo " ".$obj['gold']."</td></tr>";
		
			echo "<tr><td><img src='img/Silber.gif'/> <b>Silber</b></td><td> ";
			echo " ".$obj['silber']."</td></tr>";
		
			echo "<tr><td><img src='img/Bronze.gif'/> <b>Bronze</b></td><td> ";
			echo " ".$obj['bronze']."</td></tr>";


		
		$query = "SELECT user, id FROM user"; 

		$showdata->Query($query);	

?>
</table>
<div class="panel-body">
		<?php
		if (checkaccess("GAST") AND USER_ID == $_GET[GET_ACTION_NAME]) { ?>
		<a href="<?php echo $_SERVER["PHP_SELF"].'?'.GET_MODULE_NAME.'=send&id='.USER_ID; ?>"><button type="button" class="btn btn-default" >Barren senden</button></a>
		<a href="<?php echo $_SERVER["PHP_SELF"].'?'.GET_MODULE_NAME.'=trade&id='.USER_ID; ?>"><button type="button" class="btn btn-default">Umtauschen</button></a>
		<?php	}
			
		if (checkaccess("ADMIN")) {
		// admin form to set all	
		}
		?> <table class="table">
		<h3>Bisherige Transaktionen</h3></div><?php
		$query = "SELECT * FROM changelog WHERE username = ".$_GET[GET_ACTION_NAME]." ORDER BY timestamp DESC"; 
		$showchange = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
		$result = $showchange->Query($query);
		
		if ($showchange->ResultExists()) {
			if ($showchange->GetResult() != null) {
			// list changelog
			echo "<tr>
					<th>Zeitpunkt</th>
					<th>Art</th>
					<th>Anzahl</th>
					<th>Grund</th>
				</tr>";
			
				foreach($showchange->GetResult() as $obj) {
						echo "<tr>";
							$date =	date("d.m.Y H:i", strtotime($obj['timestamp']));
							echo "<td>$date</td>";
						
							echo "<td><img src='img/".ucfirst($obj['changed']).".gif'/>".ucfirst($obj['changed'])."</td>";
						
							$amount = $obj['amount'];
						
							echo "<td class=";
						
							echo ($amount < 0) ? "neg" : "pos";
							
							$amount = sprintf('%+g',$amount);
							echo ">$amount</td>";
							
							echo "<td>".changeVocalOutput($obj['reason'])."</td>";
						echo "</tr>";
				}
			} else {
				echo "<tr><td>Status</td><td>No data to display</td></tr>";
			}
		} else {
				echo "<tr><td>Status</td><td>No data to display</td></tr>";
			}
	}
	}
	
?>
</table>