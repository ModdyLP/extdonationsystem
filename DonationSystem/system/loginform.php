<?php if(!defined('SCRIPT_VALID')) DIE("Du hast keine Berechtigung!"); /*Prüft ob es mit index.php geöffnet wurde*/?>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
		<div class="form-group">
			<label for="username">Username</label>
			<input class="form-control" type="text" name="username" required>
		</div>
        <div class="form-group">
			<label for="password">Passwort</label>
			<input class="form-control" type="password" name="password" required>
		</div>
		<button type="submit" name="send" value="Login" class="btn btn-default">Login</button>
    </form>
