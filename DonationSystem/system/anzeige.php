<?php
if ($parent['id'] != null) {
	if ($parent["state"] >= $parent["goal"]) {
		if (checkaccess("GAST") AND !checkaccess("ADMIN")) {
			if ($state >= $obj['goal']) {
				echo changeVocalOutput("<tr class='success'><td>".$obj["name"]."<br /><span class='label label-success'>".$parentname."</span></td><td>".$state." / ".$obj["goal"]." <img src='img/".$obj["art"].".gif' /> ".$obj["art"]."<br/><span class='label label-primary'>Endet am ".$enddate[$obj['id']]."</span></td><td><button class='btn btn-success' disabled='disabled'>Bereits freigeschaltet</button></td></tr>");
			} else {
				echo changeVocalOutput("<tr><td>".$obj["name"]."<br /><span class='label label-success'>".$parentname."</span></td><td>".$state." / ".$obj["goal"]." <img src='img/".$obj["art"].".gif' /> ".$obj["art"]."</td><td><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=addrank&id=".$obj['id']."'><button class='btn btn-success'>Hierzu Beitragen</button></a></td></tr>");
			}
		}
		if(checkaccess("ADMIN")) {
			if ($state >= $obj['goal']) {
				echo changeVocalOutput("<tr class='success'><td>".$obj["name"]."<br /><span class='label label-success'>".$parentname."</span></td><td>".$state." / ".$obj["goal"]." <img src='img/".$obj["art"].".gif' /> ".$obj["art"]."<br/><span class='label label-primary'>Endet am ".$enddate[$obj['id']]."</span></td><td><button class='btn btn-success' disabled='disabled'>Bereits freigeschaltet</button></td><td><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=deleterank&id=".$obj['id']."'><button class='btn btn-danger'>Löschen</button></a></td></tr>");
			} else {
				echo changeVocalOutput("<tr><td>".$obj["name"]."<br /><span class='label label-success'>".$parentname."</span></td><td>".$state." / ".$obj["goal"]." <img src='img/".$obj["art"].".gif' /> ".$obj["art"]."</td><td><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=addrank&id=".$obj['id']."'><button class='btn btn-success'>Hierzu Beitragen</button></td><td></a><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=deleterank&id=".$obj['id']."'><button class='btn btn-danger'>Löschen</button></a></td></tr>");
			}
		} 
		if (!checkaccess("GAST")) {
			if ($state >= $obj['goal']) {
				echo changeVocalOutput("<tr class='success'><td>".$obj["name"]."<br /><span class='label label-success'>".$parentname."</span></td><td>".$state." / ".$obj["goal"]." <img src='img/".$obj["art"].".gif' /> ".$obj["art"]."<br/><span class='label label-primary'>Endet am ".$enddate[$obj['id']]."</span></td><td><button class='btn btn-success' disabled='disabled'>Bereits freigeschaltet</button></td></tr>");
			} else {
				echo changeVocalOutput("<tr><td>".$obj["name"]."<br /><span class='label label-success'>".$parentname."</span></td><td>".$state." / ".$obj["goal"]." <img src='img/".$obj["art"].".gif' /> ".$obj["art"]."</td><td><button class='btn btn-success' disabled='disabled'>Bitte einloggen</button></td></tr>");
			}
		}
	} else {
		if (checkaccess("GAST") AND !checkaccess("ADMIN")) {
			if ($state >= $obj['goal']) {
				echo changeVocalOutput("<tr class='success'><td>".$obj["name"]."<br /><span class='label label-danger'>".$parentname."</span></td><td>".$state." / ".$obj["goal"]." <img src='img/".$obj["art"].".gif' /> ".$obj["art"]."<br/><span class='label label-primary'>Endet am ".$enddate[$obj['id']]."</span></td><td><button class='btn btn-success' disabled='disabled'>Bereits freigeschaltet</button></td></tr>");
			} else {
				echo changeVocalOutput("<tr><td>".$obj["name"]."<br /><span class='label label-danger'>".$parentname."</span></td><td>".$state." / ".$obj["goal"]." <img src='img/".$obj["art"].".gif' /> ".$obj["art"]."</td><td><button class='btn btn-success' disabled='disabled'>Vorrausetzung nicht erfüllt</button></td></tr>");
			}
		}
		if(checkaccess("ADMIN")) {
			if ($state >= $obj['goal']) {
				echo changeVocalOutput("<tr class='success'><td>".$obj["name"]."<br /><span class='label label-danger'>".$parentname."</span></td><td>".$state." / ".$obj["goal"]." <img src='img/".$obj["art"].".gif' /> ".$obj["art"]."<br/><span class='label label-primary'>Endet am ".$enddate[$obj['id']]."</span></td><td><button class='btn btn-success' disabled='disabled'>Bereits freigeschaltet</button></td><td><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=deleterank&id=".$obj['id']."'><button class='btn btn-danger'>Löschen</button></a></td></tr>");
			} else {
				echo changeVocalOutput("<tr><td>".$obj["name"]."<br /><span class='label label-danger'>".$parentname."</span></td><td>".$state." / ".$obj["goal"]." <img src='img/".$obj["art"].".gif' /> ".$obj["art"]."</td><td><button class='btn btn-success' disabled='disabled'>Vorrausetzung nicht erfüllt</button></td><td></a><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=deleterank&id=".$obj['id']."'><button class='btn btn-danger'>Löschen</button></a></td></tr>");
			}
		} 
		if (!checkaccess("GAST")) {
			if ($state >= $obj['goal']) {
				echo changeVocalOutput("<tr class='success'><td>".$obj["name"]."<br /><span class='label label-danger'>".$parentname."</span></td><td>".$state." / ".$obj["goal"]." <img src='img/".$obj["art"].".gif' /> ".$obj["art"]."<br/><span class='label label-primary'>Endet am ".$enddate[$obj['id']]."</span></td><td><button class='btn btn-success' disabled='disabled'>Bereits freigeschaltet</button></td></tr>");
			} else {
				echo changeVocalOutput("<tr><td>".$obj["name"]."<br /><span class='label label-danger'>".$parentname."</span></td><td>".$state." / ".$obj["goal"]." <img src='img/".$obj["art"].".gif' /> ".$obj["art"]."</td><td><button class='btn btn-success' disabled='disabled'>Bitte einloggen</button></td></tr>");
			}
		}
	}
} else {
	if (checkaccess("GAST") AND !checkaccess("ADMIN")) {
			if ($state == $obj['goal']) {
				echo changeVocalOutput("<tr class='success'><td>".$obj["name"]."<br /><span class='label label-info'>".$parentname."</span></td><td>".$state." / ".$obj["goal"]." <img src='img/".$obj["art"].".gif' /> ".$obj["art"]."<br/><span class='label label-primary'>Endet am ".$enddate[$obj['id']]."</span></td><td><button class='btn btn-success' disabled='disabled'>Bereits freigeschaltet</button></td></tr>");
			} else {
				echo changeVocalOutput("<tr><td>".$obj["name"]."<br /><span class='label label-info'>".$parentname."</span></td><td>".$state." / ".$obj["goal"]." <img src='img/".$obj["art"].".gif' /> ".$obj["art"]."</td><td><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=addrank&id=".$obj['id']."'><button class='btn btn-success'>Hierzu Beitragen</button></a></td></tr>");
			}
		}
		if(checkaccess("ADMIN")) {
			if ($state >= $obj['goal']) {
				echo changeVocalOutput("<tr class='success'><td>".$obj["name"]."<br /><span class='label label-info'>".$parentname."</span></td><td>".$state." / ".$obj["goal"]." <img src='img/".$obj["art"].".gif' /> ".$obj["art"]."<br/><span class='label label-primary'>Endet am ".$enddate[$obj['id']]."</span></td><td><button class='btn btn-success' disabled='disabled'>Bereits freigeschaltet</button></td><td><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=deleterank&id=".$obj['id']."'><button class='btn btn-danger'>Löschen</button></a></td></tr>");
			} else {
				echo changeVocalOutput("<tr><td>".$obj["name"]."<br /><span class='label label-info'>".$parentname."</span></td><td>".$state." / ".$obj["goal"]." <img src='img/".$obj["art"].".gif' /> ".$obj["art"]."</td><td><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=addrank&id=".$obj['id']."'><button class='btn btn-success'>Hierzu Beitragen</button></td><td></a><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=deleterank&id=".$obj['id']."'><button class='btn btn-danger'>Löschen</button></a></td></tr>");
			}
		} 
		if (!checkaccess("GAST")) {
			if ($state >= $obj['goal']) {
				echo changeVocalOutput("<tr class='success'><td>".$obj["name"]."<br /><span class='label label-info'>".$parentname."</span></td><td>".$state." / ".$obj["goal"]." <img src='img/".$obj["art"].".gif' /> ".$obj["art"]."<br/><span class='label label-primary'>Endet am ".$enddate[$obj['id']]."</span></td><td><button class='btn btn-success' disabled='disabled'>Bereits freigeschaltet</button></td></tr>");
			} else {
				echo changeVocalOutput("<tr><td>".$obj["name"]."<br /><span class='label label-info'>".$parentname."</span></td><td>".$state." / ".$obj["goal"]." <img src='img/".$obj["art"].".gif' /> ".$obj["art"]."</td><td><button class='btn btn-success' disabled='disabled'>Bitte einloggen</button></td></tr>");
			}
		}
}
?>