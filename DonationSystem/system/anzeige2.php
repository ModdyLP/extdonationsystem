<?php
if ($parent1['id'] != null) {
	if ($stateparent >= $parent1["goal"]) {
		if (checkaccess("GAST") AND !checkaccess("ADMIN")) {
			if ($state1 >= $obj2['goal']) {
				echo changeVocalOutput("<tr class='success'><td>".$obj2["name"]."<br /><span class='label label-success'>".$parentname1."</span></td><td>".$state1." / ".$obj2["goal"]." <img src='img/".$obj2["art"].".gif' /> ".$obj2["art"]."<br/><span class='label label-primary'>Endet ".$enddate2[$obj2['id']]."</span></td><td><button class='btn btn-success' disabled='disabled'>Bereits freigeschaltet</button></td></tr>");
			} else {
				echo changeVocalOutput("<tr><td>".$obj2["name"]."<br /><span class='label label-success'>".$parentname1."</span></td><td>".$state1." / ".$obj2["goal"]." <img src='img/".$obj2["art"].".gif' /> ".$obj2["art"]."</td><td><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=addrank2&id=".$obj2['id']."'><button class='btn btn-success'>Hierzu Beitragen</button></a></td></tr>");
			}
		}
		if(checkaccess("ADMIN")) {
			if ($state1 >= $obj2['goal']) {
				echo changeVocalOutput("<tr class='success'><td>".$obj2["name"]."<br /><span class='label label-success'>".$parentname1."</span></td><td>".$state1." / ".$obj2["goal"]." <img src='img/".$obj2["art"].".gif' /> ".$obj2["art"]."<br/><span class='label label-primary'>Endet ".$enddate2[$obj2['id']]."</span></td><td><button class='btn btn-success' disabled='disabled'>Bereits freigeschaltet</button></td><td><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=deleterank2&id=".$obj2['id']."'><button class='btn btn-danger'>Löschen</button></a></td></tr>");
			} else {
				echo changeVocalOutput("<tr><td>".$obj2["name"]."<br /><span class='label label-success'>".$parentname1."</span></td><td>".$state1." / ".$obj2["goal"]." <img src='img/".$obj2["art"].".gif' /> ".$obj2["art"]."</td><td><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=addrank2&id=".$obj2['id']."'><button class='btn btn-success'>Hierzu Beitragen</button></td><td></a><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=deleterank2&id=".$obj2['id']."'><button class='btn btn-danger'>Löschen</button></a></td></tr>");
			}
		} 
		if (!checkaccess("GAST")) {
			if ($state1 >= $obj2['goal']) {
				echo changeVocalOutput("<tr class='success'><td>".$obj2["name"]."<br /><span class='label label-success'>".$parentname1."</span></td><td>".$state1." / ".$obj2["goal"]." <img src='img/".$obj2["art"].".gif' /> ".$obj2["art"]."<br/><span class='label label-primary'>Endet ".$enddate2[$obj2['id']]."</span></td><td><button class='btn btn-success' disabled='disabled'>Bereits freigeschaltet</button></td></tr>");
			} else {
				echo changeVocalOutput("<tr><td>".$obj2["name"]."<br /><span class='label label-success'>".$parentname1."</span></td><td>".$state1." / ".$obj2["goal"]." <img src='img/".$obj2["art"].".gif' /> ".$obj2["art"]."</td><td><button class='btn btn-success' disabled='disabled'>Bitte einloggen</button></td></tr>");
			}
		}
	} else {
		if (checkaccess("GAST") AND !checkaccess("ADMIN")) {
			if ($state1 >= $obj2['goal']) {
				echo changeVocalOutput("<tr class='success'><td>".$obj2["name"]."<br /><span class='label label-danger'>".$parentname1."</span></td><td>".$state1." / ".$obj2["goal"]." <img src='img/".$obj2["art"].".gif' /> ".$obj2["art"]."<br/><span class='label label-primary'>Endet ".$enddate2[$obj2['id']]."</span></td><td><button class='btn btn-success' disabled='disabled'>Bereits freigeschaltet</button></td></tr>");
			} else {
				echo changeVocalOutput("<tr><td>".$obj2["name"]."<br /><span class='label label-danger'>".$parentname1."</span></td><td>".$state1." / ".$obj2["goal"]." <img src='img/".$obj2["art"].".gif' /> ".$obj2["art"]."</td><td><button class='btn btn-success' disabled='disabled'>Vorrausetzung nicht erfüllt</button></td></tr>");
			}
		}
		if(checkaccess("ADMIN")) {
			if ($state1 >= $obj2['goal']) {
				echo changeVocalOutput("<tr class='success'><td>".$obj2["name"]."<br /><span class='label label-danger'>".$parentname1."</span></td><td>".$state1." / ".$obj2["goal"]." <img src='img/".$obj2["art"].".gif' /> ".$obj2["art"]."<br/><span class='label label-primary'>Endet ".$enddate2[$obj2['id']]."</span></td><td><button class='btn btn-success' disabled='disabled'>Bereits freigeschaltet</button></td><td><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=deleterank2&id=".$obj2['id']."'><button class='btn btn-danger'>Löschen</button></a></td></tr>");
			} else {
				echo changeVocalOutput("<tr><td>".$obj2["name"]."<br /><span class='label label-danger'>".$parentname1."</span></td><td>".$state1." / ".$obj2["goal"]." <img src='img/".$obj2["art"].".gif' /> ".$obj2["art"]."</td><td><button class='btn btn-success' disabled='disabled'>Vorrausetzung nicht erfüllt</button></td><td></a><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=deleterank2&id=".$obj2['id']."'><button class='btn btn-danger'>Löschen</button></a></td></tr>");
			}
		} 
		if (!checkaccess("GAST")) {
			if ($state1 >= $obj2['goal']) {
				echo changeVocalOutput("<tr class='success'><td>".$obj2["name"]."<br /><span class='label label-danger'>".$parentname1."</span></td><td>".$state1." / ".$obj2["goal"]." <img src='img/".$obj2["art"].".gif' /> ".$obj2["art"]."<br/><span class='label label-primary'>Endet ".$enddate2[$obj2['id']]."</span></td><td><button class='btn btn-success' disabled='disabled'>Bereits freigeschaltet</button></td></tr>");
			} else {
				echo changeVocalOutput("<tr><td>".$obj2["name"]."<br /><span class='label label-danger'>".$parentname1."</span></td><td>".$state1." / ".$obj2["goal"]." <img src='img/".$obj2["art"].".gif' /> ".$obj2["art"]."</td><td><button class='btn btn-success' disabled='disabled'>Bitte einloggen</button></td></tr>");
			}
		}
	}
} else {
	if (checkaccess("GAST") AND !checkaccess("ADMIN")) {
			if ($state1 == $obj2['goal']) {
				echo changeVocalOutput("<tr class='success'><td>".$obj2["name"]."<br /><span class='label label-info'>".$parentname1."</span></td><td>".$state1." / ".$obj2["goal"]." <img src='img/".$obj2["art"].".gif' /> ".$obj2["art"]."<br/><span class='label label-primary'>Endet ".$enddate2[$obj2['id']]."</span></td><td><button class='btn btn-success' disabled='disabled'>Bereits freigeschaltet</button></td></tr>");
			} else {
				echo changeVocalOutput("<tr><td>".$obj2["name"]."<br /><span class='label label-info'>".$parentname1."</span></td><td>".$state1." / ".$obj2["goal"]." <img src='img/".$obj2["art"].".gif' /> ".$obj2["art"]."</td><td><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=addrank2&id=".$obj2['id']."'><button class='btn btn-success'>Hierzu Beitragen</button></a></td></tr>");
			}
		}
		if(checkaccess("ADMIN")) {
			if ($state1 >= $obj2['goal']) {
				echo changeVocalOutput("<tr class='success'><td>".$obj2["name"]."<br /><span class='label label-info'>".$parentname1."</span></td><td>".$state1." / ".$obj2["goal"]." <img src='img/".$obj2["art"].".gif' /> ".$obj2["art"]."<br/><span class='label label-primary'>Endet ".$enddate2[$obj2['id']]."</span></td><td><button class='btn btn-success' disabled='disabled'>Bereits freigeschaltet</button></td><td><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=deleterank2&id=".$obj2['id']."'><button class='btn btn-danger'>Löschen</button></a></td></tr>");
			} else {
				echo changeVocalOutput("<tr><td>".$obj2["name"]."<br /><span class='label label-info'>".$parentname1."</span></td><td>".$state1." / ".$obj2["goal"]." <img src='img/".$obj2["art"].".gif' /> ".$obj2["art"]."</td><td><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=addrank2&id=".$obj2['id']."'><button class='btn btn-success'>Hierzu Beitragen</button></td><td></a><a href='".$_SERVER['PHP_SELF']."?".GET_MODULE_NAME."=deleterank2&id=".$obj2['id']."'><button class='btn btn-danger'>Löschen</button></a></td></tr>");
			}
		} 
		if (!checkaccess("GAST")) {
			if ($state1 >= $obj2['goal']) {
				echo changeVocalOutput("<tr class='success'><td>".$obj2["name"]."<br /><span class='label label-info'>".$parentname1."</span></td><td>".$state1." / ".$obj2["goal"]." <img src='img/".$obj2["art"].".gif' /> ".$obj2["art"]."<br/><span class='label label-primary'>Endet ".$enddate2[$obj2['id']]."</span></td><td><button class='btn btn-success' disabled='disabled'>Bereits freigeschaltet</button></td></tr>");
			} else {
				echo changeVocalOutput("<tr><td>".$obj2["name"]."<br /><span class='label label-info'>".$parentname1."</span></td><td>".$state1." / ".$obj2["goal"]." <img src='img/".$obj2["art"].".gif' /> ".$obj2["art"]."</td><td><button class='btn btn-success' disabled='disabled'>Bitte einloggen</button></td></tr>");
			}
		}
}
?>