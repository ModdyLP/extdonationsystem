<?php
	require __DIR__ . '/SourceQuery/bootstrap.php';

	use xPaw\SourceQuery\SourceQuery;
	
	function sendcommand($command) {// For the sake of this example
		Header( 'X-Content-Type-Options: nosniff' );
		
		// Edit this ->
		define( 'SQ_ENGINE',      SourceQuery::SOURCE );
		// Edit this <-
		
		$Query = new SourceQuery( );
		
		try
		{
			$Query->Connect(SQ_SERVER_ADDR, SQ_SERVER_PORT, SQ_TIMEOUT, SQ_ENGINE );
			
			$Query->SetRconPassword(RCON_PW);
			
			$Query->Rcon( $command );
		}
		catch( Exception $e )
		{
			echo $e->getMessage( );
		}
		
		$Query->Disconnect( );
	}
