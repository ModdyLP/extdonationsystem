<div class="panel panel-default">
				  <div class="panel-heading">
					<h3 class="panel-title">Freischaltung hinzufügen (Allgemein)</h3>
				  </div>
				 <div class="panel-body">
					<form class="form" method="post" action="<?php echo $_SERVER['PHP_SELF'].'?'.GET_MODULE_NAME.'=createrank'; ?>">
						<div class="form-group">
							<label for="name">Name der Freischaltung</label>
							<input class="form-control" type="text" name="name"/>
						</div>
						<div class="form-group">
							<label for="catoid">Katerogie</label>
							<select class="form-control" name="catoid">
								<?php 
									$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
									$getdata->Query("SELECT * FROM dcato;");
									if ($getdata->ResultExists()) {
										foreach($getdata->GetResult() as $obj) {
											echo "<option value='".$obj["id"]."'>".$obj['Name']."</option>";
										}
									}
								
								?>
							</select>
						</div>
						<div class="form-group">
							<label for="require">Vorraussetzung</label>
							<select class="form-control" name="require">
								<option value='false'>Keine Vorraussetzung</option>
								<?php 
									$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
									$getdata->Query("SELECT * FROM dranks;");
									if ($getdata->ResultExists()) {
										foreach($getdata->GetResult() as $obj) {
											echo "<option value='".$obj["id"]."'>".$obj['name']."</option>";
										}
									}
								
								?>
							</select>
						</div>
						<div class="form-group">
							<label for="amount">Währung</label>
							<select class="form-control" name="art">
								<option value="Gold">Gold</option>
								<option value="Silber">Silber</option>
								<option value="Bronze">Bronze</option>	
							</select>
						</div>
						<div class="form-group">
							<label for="amount">Ziel</label>
							<input class="form-control" type="text" name="amount"/>
						</div>
						<div class="form-group">
							<label for="command">Command (Erfüllung) Variablen: @a, all für alle Spieler</label>
							<input class="form-control" type="text" name="command"/>
						</div>
						<div class="form-group">
							<label for="dcommand">Command (Deaktivierung) Variablen: @a, all für alle Spieler</label>
							<input class="form-control" type="text" name="dcommand"/>
						</div>
						<button type="submit" name="send2" value="senden" class="btn btn-primary">Senden</button>
					</form>
				</div>
		</div>