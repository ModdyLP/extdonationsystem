<?php if(!defined('SCRIPT_VALID')) DIE("Du hast keine Berechtigung!"); /*Pr�ft ob es mit index.php ge�ffnet wurde*/?>
<?php
session_start();
    if(!isset($_GET[GET_MODULE_NAME]))
    {
		include "system/show.php";
    }
	if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "detail" AND isset($_GET[GET_ACTION_NAME]))
    {
		include "system/detail.php";
	}
	if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "buy")
    {
		include "system/buy.php";
	}
	if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "ranks")
    {
		include "system/ranks.php";
	}
	if (checkaccess("GAST")) {
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "send" AND !isset($_GET[GET_ACTION_NAME]))
		{
			include "system/sendform.php";
		}
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "send" AND isset($_GET[GET_ACTION_NAME]))
		{
			include "system/send.php";
		}
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "trade" AND !isset($_GET[GET_ACTION_NAME]))
		{
			include "system/tradeform.php";
		}
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "trade" AND isset($_GET[GET_ACTION_NAME]))
		{
			include "system/trade.php";
		}
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "changepw" AND !isset($_GET[GET_ACTION_NAME]))
		{
			include "system/passwordform.php";
		}
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "changepw" AND isset($_POST["send2"]))
		{
			changePassword();
		}
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "addrank" AND isset($_GET['id']))
		{
			include "system/addrank.php";
		}
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "addrank2" AND isset($_GET['id']))
		{
			include "system/addrank2.php";
		}
	}
	if (checkaccess("ADMIN")) {
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "payments")
		{
			include "system/payments.php";
		}
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "createrank")
		{
			if (isset($_POST['send2'])) {
				if (empty($_POST['name']) OR empty($_POST['amount']) OR empty($_POST['art']) OR empty($_POST['require']) OR empty($_POST['catoid'])) {
					echo '<div class="alert alert-danger" role="alert">Du musst alle Felder füllen</div>';
					?>
					<script type="text/javascript">
						window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php"', 3000);
					</script>
					<?php
				} else {
					if ($_POST["require"] == 'false' OR  $_POST["require"] == NULL) {
						$require = 0;
					} else {
						$require = $_POST["require"];
					}
					$command = $_POST['command'];
					$dcommand = $_POST['dcommand'];
					$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
					$getdata->Query(changeVocalInput("INSERT INTO dranks (id, name, state, goal, art, parent, timestamp, catoid, command, dcommand) VALUES (NULL, '".$_POST['name']."', '0', '".$_POST['amount']."', '".$_POST['art']."', '".$require."', NULL, '".$_POST['catoid']."', '".$command."', '".$dcommand."');"));
					if ($getdata->ResultExists()) {
						echo '<div class="alert alert-success" role="alert">Freischaltung erfolgreich hinzugefügt</div>';
						?>
						<script type="text/javascript">
							window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=ranks"', 3000);
						</script>
						<?php
					}
				}
			} else {
				include "system/createrankform.php";
			}
		}
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "addcato")
		{
			if (isset($_POST['send2'])) {
				if (empty($_POST['name'])) {
					echo '<div class="alert alert-danger" role="alert">Du musst alle Felder füllen</div>';
					?>
					<script type="text/javascript">
						window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php"', 3000);
					</script>
					<?php
				} else {
					$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
					$getdata->Query(changeVocalInput("INSERT INTO dcato (id, name) VALUES (NULL, '".$_POST['name']."');"));
					if ($getdata->ResultExists()) {
						echo '<div class="alert alert-success" role="alert">Katerogie erfolgreich hinzugefügt</div>';
						?>
						<script type="text/javascript">
							window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=ranks"', 3000);
						</script>
						<?php
					}
				}
			} else {
				include "system/addcato.php";
			}
		}
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "createrank2")
		{
			if (isset($_POST['send2'])) {
				if (empty($_POST['name']) OR empty($_POST['amount']) OR empty($_POST['art']) OR empty($_POST['require']) OR empty($_POST['catoid'])) {
					echo '<div class="alert alert-danger" role="alert">Du musst alle Felder füllen</div>';
					?>
					<script type="text/javascript">
						window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php"', 3000);
					</script>
					<?php
				} else {
					if (isset($_POST['limit'])) {
						$limit = 1;
					} else {
						$limit = 0;
					}
					if ($_POST["require"] == 'false' OR $_POST["require"] == NULL) {
						$require = 0;
					} else {
						$require = $_POST["require"];
					}
					$command = $_POST['command'];
					$dcommand = $_POST['dcommand'];
					$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
					$getdata->Query(changeVocalInput("INSERT INTO pdranks (id, name, goal, art, parent, catoid, tlimit, command, dcommand) VALUES (NULL, '".$_POST['name']."', '".$_POST['amount']."', '".$_POST['art']."', '".$require."', '".$_POST['catoid']."', '".$limit."', '".$command."', '".$dcommand."');"));
					if ($getdata->ResultExists()) {
						echo '<div class="alert alert-success" role="alert">Freischaltung erfolgreich hinzugefügt</div>';
						?>
						<script type="text/javascript">
							window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=ranks"', 3000);
						</script>
						<?php
					} else {
						echo '<div class="alert alert-danger" role="alert">Freischaltung fehlerhaft</div>';
						?>
						<script type="text/javascript">
							window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=ranks"', 3000);
						</script>
						<?php
					}
				}
			} else {
				include "system/createrankform2.php";
			}
		}
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "deleterank" AND isset($_GET['id']))
		{
					$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
					$getdata->Query("DELETE FROM dranks WHERE id = '".$_GET['id']."'");
					if ($getdata->ResultExists()) {
						echo '<div class="alert alert-success" role="alert">Freischaltung erfolgreich gelöscht</div>';
						?>
						<script type="text/javascript">
							window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=ranks"', 3000);
						</script>
						<?php
					}
		}
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "deleterank2" AND isset($_GET['id']))
		{
					$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
					$getdata->Query("DELETE FROM pdranks WHERE id = '".$_GET['id']."'");
					if ($getdata->ResultExists()) {
						echo '<div class="alert alert-success" role="alert">Freischaltung erfolgreich gelöscht</div>';
					}
					$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
					$getdata->Query("DELETE FROM rankusercon WHERE rankid = '".$_GET['id']."'");
					if ($getdata->ResultExists()) {
						echo '<div class="alert alert-success" role="alert">Userdaten erfolgreich gelöscht</div>';
						?>
						<script type="text/javascript">
							window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=ranks"', 3000);
						</script>
						<?php
					}
		}
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "deletelogs")
		{
				$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
				$getdata->Query("SELECT timestamp FROM changelog WHERE timestamp < NOW() - INTERVAL 90 DAY");
				if ($getdata->GetResult()[0] != null) {
					$getdata->Query("DELETE FROM changelog WHERE timestamp < NOW() - INTERVAL 90 DAY");
					echo '<div class="alert alert-success" role="alert">Löschen erfolgreich</div>';
					?>
					<script type="text/javascript">
						window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php"', 3000);
					</script>
					<?php
				} else {
					echo '<div class="alert alert-danger" role="alert">Keine Logs älter als 90 Tage</div>';
					?>
					<script type="text/javascript">
						window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php"', 3000);
					</script>
					<?php
				}
		}
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "addingot" AND isset($_GET['id']))
		{
			if (isset($_POST["send2"])) {
				$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
				$getdata->Query("SELECT * FROM duser WHERE id = '".$_GET['id']."';");
				if ($getdata->ResultExists()) {
					$user = $getdata->GetResult()[0];
					$art = $_POST['art'];
					$newamount = $user[$art]+$_POST['amount'];
					$getdata->Query("UPDATE duser SET ".$art." = '".$newamount."' WHERE id = '".$_GET['id']."'");
					$getdata->Query("INSERT INTO changelog (id, username, changed, amount, reason, timestamp) VALUES (NULL, '".$user['id']."', '".$art."', '+".$_POST['amount']."', 'Admin: Hinzugefügt', CURRENT_TIMESTAMP)");
					echo '<div class="alert alert-success" role="alert">Hinzufügen erfolgreich</div>';
					?>
					<script type="text/javascript">
						window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=detail&action=<?php echo $_GET['id']; ?>"', 3000);
					</script>
					<?php
				}
			} else {
				include "system/addingotform.php";
			}
		}
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "removeingot" AND isset($_GET['id']))
		{
			if (isset($_POST["send2"])) {
				$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
				$getdata->Query("SELECT * FROM duser WHERE id = '".$_GET['id']."';");
				if ($getdata->ResultExists()) {
					$user = $getdata->GetResult()[0];
					$art = $_POST['art'];
					$newamount = $user[$art]-$_POST['amount'];
					$getdata->Query("UPDATE duser SET ".$art." = '".$newamount."' WHERE id = '".$_GET['id']."'");
					$getdata->Query("INSERT INTO changelog (id, username, changed, amount, reason, timestamp) VALUES (NULL, '".$user['id']."', '".$art."', '-".$_POST['amount']."', 'Admin: Entfernt', CURRENT_TIMESTAMP)");
					echo '<div class="alert alert-success" role="alert">Entfernen erfolgreich</div>';
					?>
					<script type="text/javascript">
						window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=detail&action=<?php echo $_GET['id']; ?>"', 3000);
					</script>
					<?php
				}
			} else {
				include "system/removeingotform.php";
			}
		}
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "delete" AND isset($_GET['id']))
		{
			$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
			$getdata->Query("SELECT * FROM duser WHERE id = '".$_GET['id']."';");
			if ($getdata->ResultExists()) {
				$user = $getdata->GetResult()[0];
				$getdata->Query("DELETE FROM duser WHERE id = '".$_GET['id']."';");
				echo '<div class="alert alert-success" role="alert">Löschen erfolgreich</div>';
				?>
				<script type="text/javascript">
					window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php"', 3000);
				</script>
				<?php
			}
		}
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "activate" AND isset($_GET['id']))
		{
			$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
			$getdata->Query("SELECT * FROM duser WHERE id = '".$_GET['id']."';");
			if ($getdata->ResultExists()) {
				$user = $getdata->GetResult()[0];
				$getdata->Query("UPDATE duser SET active = '1' WHERE id = '".$_GET['id']."'");
				echo '<div class="alert alert-success" role="alert">Aktivieren erfolgreich</div>';
				?>
				<script type="text/javascript">
					window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=detail&action=<?php echo $_GET['id']; ?>""', 3000);
				</script>
				<?php
			}
		}
		if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "deactivate" AND isset($_GET['id']))
		{
			$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
			$getdata->Query("SELECT * FROM duser WHERE id = '".$_GET['id']."';");
			if ($getdata->ResultExists()) {
				$user = $getdata->GetResult()[0];
				$getdata->Query("UPDATE duser SET active = '0' WHERE id = '".$_GET['id']."'");
				echo '<div class="alert alert-success" role="alert">Deaktivieren erfolgreich</div>';
				?>
				<script type="text/javascript">
					window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php?module=detail&action=<?php echo $_GET['id']; ?>""', 3000);
				</script>
				<?php
			}
		}
	}
	if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "register" AND isset($_POST["send2"]))
    {
		echo "<h1>Registrierungsdetails</h1>";
		$amount = str_replace("GB","", $_SESSION["item_number"]);
		$menge = $amount;
		$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
		$getdata->Query("SELECT * FROM duser WHERE username = '".validate($_POST['username'])."';");
		if ($getdata->ResultExists()) {
			$user = $getdata->GetResult()[0];
		}
		if (empty($_POST["passwordcheck"]) AND validate($_POST["username"]) == $user["username"]) {
			$action = "login";
		} else if (validate($_POST["password"]) == validate($_POST["passwordcheck"])) {
			$action = "register";
		} else {
			echo '<div class="alert alert-danger" role="alert">Da ist dir wohl ein Fehler unterlaufen.</div>';
			include "system/registerform.php";
			die();
		}
		if ($action == "login") {
			if (encrypt_password(validate($_POST["password"])) == $user["password"]) {
				$amount = $user["gold"]+$amount;
				$query = changeVocalInput("UPDATE duser SET gold = '".$amount."', active = '1' WHERE  username = '".validate($_POST["username"])."';");
			} else {
			echo '<div class="alert alert-danger" role="alert">Benutzername oder Passwort fehlerhaft.</div>';
			include "system/registerform.php";
			die();
			}
		} else if($action == "register") {
			if ($user["username"] != null) {
				echo '<div class="alert alert-danger" role="alert">Dieser Benutzer ist vorhanden. Bitte lasse die Passwort Wiederholung dann frei.</div>';
				include "system/registerform.php";
				die();
			} else {
				$password = encrypt_password($_POST["password"]);
				$query = changeVocalInput("INSERT INTO duser (id, username, email, password, keystamp, keytstamp, gold, silber, bronze, role, active) VALUES (NULL, '".validate($_POST['username'])."', '".validate($_POST['email'])."', '".$password."', '' , '".time()."', '".$amount."', '0', '0', '0', '1');");
			}
		} else {
			echo '<div class="alert alert-danger" role="alert">Da ist dir wohl ein Fehler unterlaufen</div>';
			include "system/registerform.php";
			die();
		}
		$savedata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
		$savedata->Query($query);
		$savedata->Query("SELECT * FROM duser WHERE username = '".validate($_POST["username"])."';");
		if ($savedata->ResultExists()) {
			$obj = $savedata->GetResult()[0];
			$query2 = "INSERT INTO changelog (id, username, changed, amount, reason, timestamp) VALUES (NULL, '".$obj['id']."', 'Gold', '+".$menge."', 'Donation', CURRENT_TIMESTAMP)";
			$savedata->Query($query2);
		}
		if ($savedata->ResultExists()) {
			echo '<div class="alert alert-success" role="alert">Speicherung des Goldes war erfolgreich</div>';
			include "system/Rcon.php";
			sendcommand("ancientdonations web Donation ".validate($_POST["username"])." ".$amount);
			?>
			<script type="text/javascript">
				window.setTimeout('location.href="<?php echo BASE_URL; ?>/index.php"', 3000);
			</script>
			<?php
		} else {
			echo '<div class="alert alert-danger" role="alert">Speicherung des Goldes war fehlerhaft</div>';
			include "system/registerform.php";
			die();
		}
	}
	if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "registeruser" AND isset($_POST["send2"]))
    {
		include "system/register.php";
	}
	if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "registeruser" AND !isset($_POST["send2"]))
    {
		include "system/registerform2.php";
	}
	if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "success")
    {
		$sessionid = str_replace("'","", $_GET["sessionid"]);
		if ($sessionid == session_id()) {
			if ($_POST["txn_id"] != null) {
				echo '<div class="alert alert-success" role="alert">Zahlung erfolgreich</div>';
				echo "Du erhälst: ".$_POST["item_name"];
				$savedata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
				$savedata->Query("INSERT INTO payments (id, txn_id, buyeremail, buyername, item_number, date) VALUES (NULL, '".$_POST["txn_id"]."', '".$_POST["payer_email"]."', '".$_POST["first_name"]." ".$_POST["last_name"]."', '".$_POST["item_number"]."', '".$_POST["payment_date"]."');");
				$_SESSION['item_number'] = $_POST["item_number"];
				$_SESSION['payer_email'] = $_POST["payer_email"];
				include "system/registerform.php";
			} else {
				echo '<div class="alert alert-danger" role="alert">Ein Fehler ist aufgetreten</div>';
			}	
		} else {
				echo '<div class="alert alert-danger" role="alert">Ein Fehler ist aufgetreten</div>';
			}
	}
	if(isset($_GET[GET_MODULE_NAME]) AND $_GET[GET_MODULE_NAME] == "error")
    {
		echo '<div class="alert alert-danger" role="alert">Der Bezahlvorgang wurde abgebrochen oder nicht richtig druchgeführt. Bitte wende dich an den Support</div>';
	}
?>



   

