<?php if(!defined('SCRIPT_VALID')) DIE("Du hast keine Berechtigung!"); /*Prüft ob es mit index.php geöffnet wurde*/
session_start();
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Kaufe Barren und unterstütze und damit</h3>
  </div>
  <div class="panel-body">
	<table class="table">
		<tr><th>Menge</th><th></th></tr>
		<tr><td>1000 <img src="img/Gold.gif"/> Goldbarren</td><td>
			<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
			  <!-- Saved buttons use the "secure click" command -->
			  <input type="hidden" name="cmd" value="_s-xclick">

			  <!-- Saved buttons are identified by their button IDs -->
			  <input type="hidden" name="hosted_button_id" value="SSBZUXLUVTNPE">
			  <input type="hidden" name="sessionid" value="<?php echo session_id(); ?>">
			  <input type="hidden" name="item_name" value="1000 Goldbarren">
			  <input type="hidden" name="item_number" value="1000GB">
			  <input type="hidden" name="amount" value="10.00">
			  <input type="hidden" name="notify_url" value="<?php echo BASE_URL; ?>/index.php/system/ipn.php">
			  <input type="hidden" name="return" value="<?php echo BASE_URL; ?>/index.php?module=success&sessionid='<?php echo session_id(); ?>'">
			  <input type="hidden" name="rm" value="2">
			  <input type="hidden" name="cbt" value="Zurück zur Website, um Belohnung zu speichern">
			  <input type="image" name="submit"
				src="https://www.paypalobjects.com/de_DE/DE/i/btn/btn_donate_LG.gif"
				alt="PayPal - The safer, easier way to pay online">
			</form>
		</td></tr>
		<tr><td>5000 <img src="img/Gold.gif"/> Goldbarren</td><td>
			<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
			  <!-- Saved buttons use the "secure click" command -->
			  <input type="hidden" name="cmd" value="_s-xclick">

			  <!-- Saved buttons are identified by their button IDs -->
			  <input type="hidden" name="hosted_button_id" value="7P8N4C3U3GEX6">
			  <input type="hidden" name="sessionid" value="<?php echo session_id(); ?>">
			  <input type="hidden" name="item_name" value="5000 Goldbarren">
			  <input type="hidden" name="item_number" value="5000GB">
			  <input type="hidden" name="amount" value="50.00">
			  <input type="hidden" name="notify_url" value="<?php echo BASE_URL; ?>/index.php/system/ipn.php">
			  <input type="hidden" name="return" value="<?php echo BASE_URL; ?>/index.php/index.php?module=success&sessionid='<?php echo session_id(); ?>'">
			  <input type="hidden" name="rm" value="2">
			  <input type="hidden" name="cbt" value="Zurück zur Website, um Belohnung zu speichern">
			  <input type="image" name="submit"
				src="https://www.paypalobjects.com/de_DE/DE/i/btn/btn_donate_LG.gif"
				alt="PayPal - The safer, easier way to pay online">
			</form>
		</td></tr>
	</table>
  </div>
</div>


