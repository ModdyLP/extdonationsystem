﻿<?php if(!defined('SCRIPT_VALID')) DIE("Du hast keine Berechtigung!"); /*Prüft ob es mit index.php geöffnet wurde*/?>
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Tauschkurse</h3>
  </div>
  <div class="panel-body">
    1 Gold = 10 Silber
	1 Silber = 10 Bronze
	20 Bronze = 1 Silber
	20 Silber = 1 Gold
  </div>
</div>
<form class="form-horizontal" method="post" id="form_trade" action="<?php echo $_SERVER['PHP_SELF'].'?'.GET_MODULE_NAME.'=trade&'.GET_ACTION_NAME.'=trade&id='.USER_ID; ?>">
			<div class="form-group">
					<label for="from">Von</label>
						<select class="form-control" name="from">
							<option>Gold</option>
							<option>Silber</option>
							<option>Bronze</option>	
						</select>
			</div>
			<div class="form-group">
					<label for="to">Nach</label>
						<select  class="form-control" name="to">
							<option>Gold</option>
							<option>Silber</option>
							<option>Bronze</option>	
						</select>
			</div>
			<div class="form-group">
					<label for="to">Anzahl</label>
					<input class="form-control" type="text" name="amount"/>
			</div>
			<button type="submit" id="send" value="senden" class="btn btn-primary">Umtauschen</button>
		</form>