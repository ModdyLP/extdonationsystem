<?php
		ERROR_REPORTING(0);
		define("SCRIPT_VALID", "Is Valid");
		
		// ATTENTION: PLEASE EDIT THIS LINE TO GET THE RIGHT URL 
		define("BASE_URL", "http://$_SERVER[HTTP_HOST]/my%20portable%20files/websitetest");
		
		foreach(glob("settings/*.php") as $pathname)
		{
			include_once $pathname;
		}
		foreach(glob("core/*.php") as $pathname)
		{
			include_once $pathname;
		}
		$getdata = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
		$getdata->Query("UPDATE duser SET active = '0' WHERE timestamp < NOW() - INTERVAL 90 DAY");
		?>
<!doctype html>
<html>
	<head>
		<title>Donationsystem</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		
		<?php
		foreach(glob("css/*.css") as $pathname)
			{
				echo '<link rel="stylesheet" type="text/css" href="'.$pathname.'"/>';
			}
		?>
	</head>
	<body>
	<div id="wrapper" class="container-fluid">
		<div class="row">
			<div id="navi" class="col-md-4">
			<div class="panel panel-default">
				  <div class="panel-heading">
					<h3 class="panel-title">Login</h3>
				  </div>
				  <div class="panel-body">
					<?php
						include "./system/notmain.php";
						?>
				  </div>
			</div>
			<div class="panel panel-default">
				  <div class="panel-heading">
					<h3 class="panel-title">Navigation</h3>
				  </div>
				  <div class="panel-body">
					<div class="sidebar-nav">
					  <div class="navbar navbar-default" role="navigation">
						<div class="navbar-header">
						  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
							<span class="sr-only">Aktiviere Navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
						  <span class="visible-xs navbar-brand">Menu</span>
						</div>
						<div class="navbar-collapse collapse sidebar-navbar-collapse">
						  <ul class="nav navbar-nav">
							<?php
								include_once "./system/navi.php";
							?>
						  </ul>
						</div><!--/.nav-collapse -->
					  </div>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				  <div class="panel-heading">
					<h3 class="panel-title">Währungsarten</h3>
				  </div>
				  <table class="table">
					<tr><th>Art</th><th>Verwendung</th></tr>
					<tr><th><img src="img/Gold.gif" alt="Gold" /> Gold</th><td>Donationwährung<br /></td></tr>
					<tr><th><img src="img/Silber.gif" alt="Silber" /> Silber</th><td>Lohnwährung<br /></td></tr>
					<tr><th><img src="img/Bronze.gif" alt="Bronze" /> Bronze</th><td>Tauschwährung<br /></td></tr>
				</table>
					
			</div>
			<?php if (checkaccess("ADMIN")) { ?>
			<div class="panel panel-default">
				  <div class="panel-heading">
					<h3 class="panel-title">Admin Menü</h3>
				  </div>
					<div class="panel-body">
						<div class="sidebar-nav">
					  <div class="navbar navbar-default" role="navigation">
						<div class="navbar-header">
						  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
							<span class="sr-only">Aktiviere Navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
						  <span class="visible-xs navbar-brand">Menu</span>
						</div>
						<div class="navbar-collapse collapse sidebar-navbar-collapse">
						  <ul class="nav navbar-nav">
							<li><a href="<?php echo $_SERVER["PHP_SELF"].'?'.GET_MODULE_NAME.'=deletelogs'; ?>">Logs löschen (älter als 90 Tage)</a></li>
							<li><a href="<?php echo $_SERVER["PHP_SELF"]."?".GET_MODULE_NAME."=payments"; ?>">PayPal Verlauf </a></li>
							<li><a href="<?php echo $_SERVER["PHP_SELF"]."?".GET_MODULE_NAME."=createrank"; ?>">Allgemeine Freischaltungen hinzufügen</a></li>
							<li><a href="<?php echo $_SERVER["PHP_SELF"]."?".GET_MODULE_NAME."=createrank2"; ?>">Persönliche Freischaltungen hinzufügen</a></li>
							<li><a href="<?php echo $_SERVER["PHP_SELF"]."?".GET_MODULE_NAME."=addcato"; ?>">Katerogie hinzufügen</a></li>
						  </ul>
						</div><!--/.nav-collapse -->
					  </div>
					</div>
					</div>
			</div>
			<?php } ?>
			</div>
			<div id="main" class="col-md-8">
			
			<?php
			foreach(glob("./system/main.php") as $pathname)
				{
					include_once $pathname;
				}

			?>
			</div>
		</div>
	</div>
	</body>
	<!--
	##############################################################Copyright###########################################
	Copyright: Donationsystem für MysticWorld 2016 written by https://moddylp.de/   ModdyLP
	##################################################################################################################
	-->
</html>
