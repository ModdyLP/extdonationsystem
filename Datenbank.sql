-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 06. Jul 2016 um 15:04
-- Server Version: 5.6.15-log
-- PHP Version: 5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `donate`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `changelog`
--

CREATE TABLE IF NOT EXISTS `changelog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` int(11) NOT NULL,
  `changed` varchar(90) NOT NULL,
  `amount` decimal(11,2) NOT NULL,
  `reason` varchar(256) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `dcato`
--

CREATE TABLE IF NOT EXISTS `dcato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(90) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `dcato`
--

INSERT INTO `dcato` (`id`, `Name`) VALUES
(1, 'Minecraft'),
(2, 'Teamspeak');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `dranks`
--

CREATE TABLE IF NOT EXISTS `dranks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(90) NOT NULL,
  `state` decimal(11,2) NOT NULL DEFAULT '0.00',
  `goal` decimal(11,2) NOT NULL DEFAULT '0.00',
  `art` varchar(20) NOT NULL DEFAULT 'Bronze',
  `parent` int(11) NOT NULL,
  `timestamp` varchar(90) DEFAULT NULL,
  `catoid` int(11) DEFAULT NULL,
  `command` varchar(250) DEFAULT NULL,
  `dcommand` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `duser`
--

CREATE TABLE IF NOT EXISTS `duser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(90) NOT NULL,
  `email` varchar(120) NOT NULL,
  `password` varchar(90) NOT NULL,
  `keystamp` varchar(90) NOT NULL,
  `keytstamp` varchar(90) NOT NULL,
  `gold` decimal(11,2) NOT NULL DEFAULT '0.00',
  `silber` decimal(11,2) NOT NULL DEFAULT '0.00',
  `bronze` decimal(11,2) NOT NULL DEFAULT '0.00',
  `role` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Daten für Tabelle `duser`
--

INSERT INTO `duser` (`id`, `username`, `email`, `password`, `keystamp`, `keytstamp`, `gold`, `silber`, `bronze`, `role`, `active`) VALUES
(1, 'Admin', 'admin[at]admin.admin', '18bdb7a947cc41b6fd815804fe09433150d190673ce89a7d332c7bbbb960d1c0', 'd32176266038f05df054cb184e052448f6d163d3f5aac1cbb046990e0c108e92', '1467810165', '0.00', '0.00', '0.00', 4, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `txn_id` varchar(90) NOT NULL,
  `buyeremail` varchar(90) NOT NULL,
  `buyername` varchar(90) NOT NULL,
  `item_number` varchar(60) NOT NULL,
  `date` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `pdranks`
--

CREATE TABLE IF NOT EXISTS `pdranks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(90) NOT NULL,
  `goal` decimal(11,2) NOT NULL DEFAULT '0.00',
  `art` varchar(20) NOT NULL DEFAULT 'Bronze',
  `parent` int(11) NOT NULL,
  `catoid` int(11) DEFAULT NULL,
  `tlimit` int(11) NOT NULL DEFAULT '0',
  `command` varchar(250) DEFAULT NULL,
  `dcommand` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rankusercon`
--

CREATE TABLE IF NOT EXISTS `rankusercon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rankid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `timestamp` varchar(90) DEFAULT NULL,
  `state` decimal(11,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rankid` (`rankid`,`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rolename` varchar(90) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `role`
--

INSERT INTO `role` (`id`, `rolename`) VALUES
(0, 'GAST'),
(4, 'ADMIN');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
